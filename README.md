# Instances de Logan

La configuration Terraform comporte le state du projet (fichier `.tfstate`)

Pour contrôler l'état de l'instance, décommenter une commande dans `instance.tf` et exécuter la commande suivante : 
```bash
terraform apply -target null_resource.reboot_instance
```

Pour contrôler les ressources, il reste nécessaire d'avoir configuré l'accès AWS dans un fichier `terraform.tfvars`.