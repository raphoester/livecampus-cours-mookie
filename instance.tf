resource "null_resource" "reboot_instance" {
  provisioner "local-exec" {
    on_failure  = fail
    interpreter = ["powershell", "-Command"]
    command     = <<EOT
        # aws ec2 reboot-instances --instance-ids ${element(aws_instance.server.*.id,count.index)} --region ${var.AWS_REGION}
        # aws ec2 start-instances --instance-ids ${element(aws_instance.server.*.id,count.index)} --region ${var.AWS_REGION}
        # aws ec2 stop-instances --instance-ids ${element(aws_instance.server.*.id,count.index)} --region ${var.AWS_REGION}
     EOT
  }

  count = var.INSTANCES_COUNT
#   this setting will trigger script every time,change it something needed
  triggers = {
    always_run = "${timestamp()}"
  }
}

resource "aws_security_group" "pub-sg-1" {
	vpc_id = aws_vpc.vpc-1.id
	name = "logan_sg"
	ingress = [
		{
			cidr_blocks = [ "0.0.0.0/0" ]
			description = "authorizes ssh trafic"
			from_port = 0
			ipv6_cidr_blocks = []
			prefix_list_ids = []
			protocol = "tcp"
			security_groups = []
			self = false
			to_port = 22
		},{
		   cidr_blocks = [ "0.0.0.0/0" ]
			description = "authorizes all icmp codes"
			from_port = -1
			ipv6_cidr_blocks = []
			prefix_list_ids = []
			protocol = "icmp"
			security_groups = []
			self = false
			to_port = -1
		},{
		   cidr_blocks = [ "0.0.0.0/0" ]
			description = "authorizes rdp"
			from_port = 0
			ipv6_cidr_blocks = []
			prefix_list_ids = []
			protocol = "tcp"
			security_groups = []
			self = false
			to_port = 3389
		}
	]
	
	egress = [
		{
			cidr_blocks = [ "0.0.0.0/0" ]
			description = "value"
			from_port = 0
			ipv6_cidr_blocks = []
			prefix_list_ids = []
			protocol = "-1"
			security_groups = []
			self = false
			to_port = 0
		}
	]
}

resource "aws_instance" "server" {
	ami = lookup(var.AMIS, var.AWS_REGION)
	associate_public_ip_address = true
	instance_type = var.INSTANCE_TYPE
	subnet_id = aws_subnet.pub-subnet-1.id
	tags = {
	  "Name" = "logan_server"
	}
	key_name = aws_key_pair.kp-server.key_name
	# security_groups = [ aws_security_group.pub-sg-1.id ]
	vpc_security_group_ids = [
		aws_security_group.pub-sg-1.id
	] 
	count = var.INSTANCES_COUNT
}

resource "tls_private_key" "prv-key" {
	algorithm = "RSA"
	rsa_bits = 4096

}

resource "aws_key_pair" "kp-server" {
	key_name = "kp_logan_server"
	public_key = tls_private_key.prv-key.public_key_openssh
}

resource "local_file" "ssh_key" {
	filename = "${aws_key_pair.kp-server.key_name}.pem"
	content = tls_private_key.prv-key.private_key_pem
}

resource "aws_eip" "eip-manager" {
	instance = element(aws_instance.server.*.id,count.index)
	count = var.INSTANCES_COUNT
	vpc = true

	tags = {
		Name = "eip-logan-${count.index + 1}"
	}
}
