variable "AWS_ACCESS_KEY"  {}
variable "AWS_SECRET_KEY"  {}
variable "AWS_REGION"  {
    default = "eu-west-2"
}

variable "AMIS" {
    type = map(string)
    default = {
        eu-west-2 = "ami-0be9a9463aaead52b"
    }
}

variable "INSTANCE_TYPE" {
    type = string
    default = "t2.micro"
}

variable INSTANCES_COUNT {
    type = number
    default = 2
}