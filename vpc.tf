variable vpc_cidr {
	default = "10.0.0.0/16"
	type = string
}

variable subnet_cidr {
	default = "10.0.0.0/24"
	type = string
}

resource "aws_vpc" "vpc-1" {
	cidr_block = "${var.vpc_cidr}"
	instance_tenancy = "default"
	enable_dns_hostnames = true
	tags = {
		Name = "logan_vpc"
	}
}

resource "aws_internet_gateway" "igw-1" {
	vpc_id = aws_vpc.vpc-1.id
	tags = {
		Name = "logan_gateway"
	}
}

resource "aws_subnet" "pub-subnet-1" {
	vpc_id = aws_vpc.vpc-1.id
	cidr_block = "${var.subnet_cidr}"
	map_public_ip_on_launch = true
	tags = {
		Name = "logan_public_subnet"
	}
	availability_zone = "eu-west-2b"
}

resource "aws_route_table" "pub-rtb-1" {
	vpc_id = aws_vpc.vpc-1.id
	route {
		cidr_block = "0.0.0.0/0"
		gateway_id = aws_internet_gateway.igw-1.id
	}
	tags = {
		Name = "Public Route Table"
	}
}

resource "aws_route_table_association" "pub-rtb-subnet-assoc-1" {
	subnet_id = aws_subnet.pub-subnet-1.id
	route_table_id = aws_route_table.pub-rtb-1.id
}
